BUILD_DIR=build/
TEST_DIR=test/

sampler_clean() {
	rm -rf $BUILD_DIR $TEST_DIR
}

sampler_coverage_print() {
	ninja -C $TEST_DIR coverage-text
	cat $TEST_DIR/meson-logs/coverage.txt
}

sampler_coverage_html() {
	ninja -C $TEST_DIR coverage-html
	xdg-open $TEST_DIR/meson-logs/coveragereport/index.html
}

sampler_cscope() {
	ninja -C $BUILD_DIR cscope
}

sampler_format() {
	ninja -C $BUILD_DIR clang-format
}

sampler_format_check() {
	ninja -C $BUILD_DIR clang-format-check
}

sampler_build() {
	ninja -C $BUILD_DIR
}

sampler_flash() {
	ninja -C $BUILD_DIR flash
}

sampler_setup() {
	meson setup $BUILD_DIR --cross-file mcu/cross.txt -Doptimization=s -Db_lto=true -Db_ndebug=true
	meson setup $TEST_DIR -Db_coverage=true
}

sampler_test() {
	meson test -C $TEST_DIR
}

sampler_documentation() {
	ninja -C $BUILD_DIR documentation/html
	xdg-open $BUILD_DIR/documentation/html/index.html
}
