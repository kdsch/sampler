#include "len.h"
#include <assert.h>
#include <errno.h>
#include <sndfile.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct table {
	const char *name;
	const char *path;
	size_t cap;
	size_t len;
	int16_t *frames;
	uint32_t frame_rate;
};

static void
usage(const char *argv0)
{
	printf("usage: %s outfile\n", argv0);
}

static void
table_render(const struct table *t, FILE *out)
{
	fprintf(out, "static const int16_t %s[] = {", t->name);

	for (size_t i = 0; i < t->len; i++) {
		fprintf(out, "%d,", t->frames[i]);
	}

	fprintf(out, "};\n");
}

static int
table_grow(struct table *t)
{
	assert(t);

	const size_t newcap = t->cap ? 2 * t->cap : 48;
	int16_t *frames = realloc(t->frames, sizeof(t->frames[0]) * newcap);

	if (!frames) {
		perror("could not grow table");
		return errno;
	}

	t->frames = frames;
	t->cap = newcap;
	return 0;
}

static void
table_init(struct table *t)
{
	assert(t);

	t->cap = 0;
	t->len = 0;
	t->frames = NULL;
}

static int16_t *
table_end(const struct table *t)
{
	assert(t);
	assert(t->len <= t->cap);

	return &t->frames[t->len];
}

static size_t
table_room(const struct table *t)
{
	assert(t);
	assert(t->len <= t->cap);

	return t->cap - t->len;
}

static void
table_load(struct table *t)
{
	assert(t);

	SF_INFO info = {.format = 0};
	SNDFILE *f = sf_open(t->path, SFM_READ, &info);

	if (sf_error(f)) {
		fprintf(stderr, "could not open '%s': %s\n", t->path, sf_strerror(f));
		return;
	}

	t->frame_rate = info.samplerate;

	for (;;) {
		if (table_room(t) == 0) {
			table_grow(t);
		}

		const size_t count = sf_read_short(f, table_end(t), table_room(t));
		t->len += count;

		if (count < table_room(t)) {
			break;
		}
	}

	sf_close(f);
}

int
main(int argc, const char *argv[])
{
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	FILE *out = fopen(argv[1], "w");

	if (!out) {
		perror("could not open outfile");
		return 1;
	}

	struct table samples[] = {
		{.path = "posix/table-generator/samples/bass.wav", .name = "bass"},
		{.path = "posix/table-generator/samples/kick.wav", .name = "kick"},
		{.path = "posix/table-generator/samples/snare.wav", .name = "snare"},
		{.path = "posix/table-generator/samples/hat.wav", .name = "hat"},
	};

	for (size_t i = 0; i < len(samples); i++) {
		table_init(&samples[i]);
		table_load(&samples[i]);
		table_render(&samples[i], out);
	}

	fprintf(out, "const struct sample samples[%lu] = {\n", len(samples));

	for (size_t i = 0; i < len(samples); i++) {
		fprintf(out, "\t{.frame_rate = %u, .len = %zu, .frames = %s},\n", samples[i].frame_rate, samples[i].len, samples[i].name);
	}

	fprintf(out, "};\n");
}
