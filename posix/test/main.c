#include "app.c"
#include "sequencer.c"
#include <stdarg.h>
#include <stdio.h>

struct test {
	int fails;
};

static void
errorf(struct test *t, const char *restrict format, ...)
{
	va_list ap;
	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
	t->fails++;
}

static void
test_ticks_per_step(struct test *t)
{
	struct cas {
		const char *desc;
		float frame_rate;
		uint16_t bpm_x10;
		float want;
	} const cases[] = {
		{"44.1kHz, 80bpm", 44.1e3, 800, 8268.75f},
		{"44.1kHz, 120bpm", 44.1e3, 1200, 5512.5f},
		{"44.1kHz, 180bpm", 44.1e3, 1800, 3675},
		{"48kHz, 80bpm", 48e3, 800, 9e3},
		{"48kHz, 120bpm", 48e3, 1200, 6e3},
		{"48kHz, 180bpm", 48e3, 1800, 4e3},
	};

	for (size_t i = 0; i < len(cases); i++) {
		const struct cas c = cases[i];
		const float got = ticks_per_step(c.frame_rate, c.bpm_x10);

		if (got != c.want) {
			errorf(t, "%s %s: got %f; want %f\n", __func__, c.desc, got, c.want);
		}
	}
}

static void
test_tick_index_update(struct test *t)
{
	struct cas {
		const char *desc;
		uint16_t index;
		float ticks_per_step;
		uint16_t want;
	} const cases[] = {
		{.desc = "zero", .index = 0, .ticks_per_step = 4, .want = 1},
		{.desc = "one", .index = 1, .ticks_per_step = 4, .want = 2},
		{.desc = "two", .index = 2, .ticks_per_step = 4, .want = 3},
		{.desc = "three", .index = 3, .ticks_per_step = 4, .want = 0},
	};

	for (size_t i = 0; i < len(cases); i++) {
		const struct cas c = cases[i];
		const uint16_t got = tick_index_update(c.index, c.ticks_per_step);

		if (got != c.want) {
			errorf(t, "%s %s: got %u; want %u\n", __func__, c.desc, got, c.want);
		}
	}
}

static void
test_app(struct test *t)
{
	struct app app;
	float buffer[48];
	app_init(&app, 48000, len(buffer));

	for (uint8_t i = 0; i < len(buffer); i++) {
		buffer[i] = app_render_audio(&app);
	}

	app_transport_state_set(&app, TRANSPORT_STATE_PLAYING);

	// 4 beats  minute   60 seconds 1000 buffers
	// ------- --------- ---------- ------------ = 1333.3 buffers/bar
	//   bar   180 beats  minute      second
	const float iterations = 4 * 60 * 1e3 * 10.0f / app.sequencer.bpm_x10;
	float power = 0;

	for (uint16_t i = 1; i < iterations; i++) {
		for (uint8_t j = 0; j < len(buffer); j++) {
			buffer[j] = app_render_audio(&app);
		}

		for (uint8_t j = 0; j < len(buffer); j++) {
			power += buffer[j] * buffer[j];
		}
	}

	if (power <= 0) {
		errorf(t, "%s: nonpositive power\n", __func__);
	}

	for (uint8_t i = 0; i < len(buffer); i++) {
		buffer[i] = app_render_audio(&app);
	}

	app_transport_state_set(&app, TRANSPORT_STATE_STOPPED);

	for (uint8_t i = 0; i < len(buffer); i++) {
		buffer[i] = app_render_audio(&app);
	}
}

int
main(void)
{
	struct test t = {.fails = 0};
	test_ticks_per_step(&t);
	test_tick_index_update(&t);
	test_app(&t);
	return t.fails;
}
