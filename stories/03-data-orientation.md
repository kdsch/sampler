# Evaluating data-oriented design

We might better describe data-oriented design (DOD) as context-sensitive
design. It asks these questions:

- What hardware does the code run on?
- What execution paths does the code most often take?
- How does the code use the data?


## What hardware does the code run on?

The project targets two platforms: POSIX and ARM Cortex M4F.

POSIX isn't a hardware platform, but it's available on many advanced
operating systems that run on advanced processors. Commonly, users
run such operating systems on x86-64 and aarch64 processors.

Advanced CPUs use multi-level caches to speed up RAM access.  They read
both instructions and data from RAM, although often use separate
first-level caches for these. Advanced CPUs have long instruction
pipelines and perform branch prediction. They often have multiple cores
and hardware threads.

ARM Cortex-M processors don't have caches for embedded or external
RAM. They cache embedded flash, which holds both instructions and
data. The processors have a larger instruction cache to speed up
instruction fetching. Cortex-M-based MCUs usually have one CPU and a
5-stage instruction pipeline, which performs branch prediction.

According to the _ARM Cortex™-M Programming Guide to Memory Barrier
Instructions_, section 3.6, "System implementation requirements",

> ## System level cache
>
> The Cortex-M0, Cortex-M0+, Cortex-M1, Cortex-M3, and Cortex-M4 processors
> do not have any internal cache memory. However, it is possible for a
> SoC design to integrate a system level cache.
>
> **Note**
> A small caching component is present in the Cortex-M3 and Cortex-M4
> processors to accelerate flash memory accesses during instruction fetches.

Therefore we have to check the specific MCU ("SoC") in question.

> The Flash memory interface manages CPU AHB I-Code and D-Code accesses
> to the Flash memory. It implements the erase and program Flash memory
> operations and the read and write protection mechanisms. It accelerates
> code execution with a system of instruction prefetch and cache lines.

Section 3.2 of RM0090 Rev 19 says that the STM32F4 has 

- 64 cache lines of 128 bits on I-Code
- 8 cache lines of 128 bits on D-Code

A 128-bit cache line equals 16 bytes.

This means there's 1KiB of instruction cache and 128 bytes of data cache.
This only applies to internal flash memory, not SRAM or external RAM.

ARM Cortex-M4 has a five-stage instruction pipeline.


## Implications for this project

We designed this software for portability rather than suitability to
concrete details, so DOD may advise us to make some changes.


### Make branches predictable

Both hardware platforms have instruction pipelines, so increasing branch
predictability can improve performance by reducing branch misses.

- Make a branch more predictable by switching paths less often.


#### Example: improve predictability through ordering

The `if`-statement in this loop exemplifies an unpredictable branch
because the condition `random[i] < T` changes often in time:

	uint32_t random[N];

	for (uint32_t i = 0; i < N; i++) {
		if (random[i] < T) {
			alpha();
		} else {
			beta();
		}
	}

We can make it predictable by sorting the array before the loop. The
condition changes only for one iteration, so the predictor easily handles
this branch.

	qsort(random, N, sizeof(random[0]), compare_uint32);

	for (uint32_t i = 0; i < N; i++) {
		if (random[i] < T) {
			alpha();
		} else {
			beta();
		}
	}


#### Example: improve predictability through specificity

The predictor will also struggle with this `if`-statement, because `i %
2` changes for each iteration of the loop:

	for (uint32_t i = 0; i < N; i++) {
		if (i % 2 == 0) {
			alpha();
		} else {
			beta();
		}
	}

Assuming that the `alpha` and `beta` calls can happen in any order,
we can eliminate the conditional:

	for (uint32_t i = 0; i < N; i += 2) {
		alpha();
	}

	for (uint32_t i = 1; i < N; i += 2) {
		beta();
	}

The instruction cache also finds this code friendlier.


### Keep associated instructions nearby

Each hardware platform has an instruction cache, so increasing the
locality of instructions can improve performance by reducing cache misses.

- Increase instruction locality by processing an array in stages.
- Increase instruction locality by reducing branch and call instructions.
- Reduce branches by replacing conditionals with data manipulation.
- Reduce call instructions by inlining.


#### Example: increase temporal locality

This code puts a lot of pressure on the instruction cache. It requires
the cache to hold three different functions at the same time:

	for (uint32_t i = 0; i < N; i++) {
		alpha();
		beta();
		gamma();
	}

We might not mind this if the functions use little space relative to the
size of the instruction cache. However, if they're big, then this will
result in thrashing. We can reduce cache misses by requiring the cache
to hold only one at a time:

	for (uint32_t i = 0; i < N; i++) {
		alpha();
	}

	for (uint32_t i = 0; i < N; i++) {
		beta();
	}

	for (uint32_t i = 0; i < N; i++) {
		gamma();
	}

This also extends to the duality between a loop over an element-processing
function and an array processing function. We could rewrite these functions
with the loop inside.


#### Example: increase spatial locality

Let's use the same example as above. If the compiler puts these functions
at addresses far away from each other, the cache likely won't hold them at
the same time. They may have poor spatial locality.

	for (uint32_t i = 0; i < N; i++) {
		alpha();
		beta();
		gamma();
	}

We can improve spatial locality by merging them together.

	for (uint32_t i = 0; i < N; i++) {
		alpha_beta_gamma();
	}


### Keep associated data nearby

x86 CPUs have a data cache, so increasing data locality can improve
performance by reducing cache misses. Data locality won't make a
difference on ARM Cortex-M4 because it has no data cache. However,
Cortex-M7 does have a data cache.

Heap allocation often leads to poor use of the data cache.


## Metrics

DOD requires observing different metrics:

- the size of structs
- the size of functions
- the number of branch misses
- the number of cache misses
- wall clock time
- how much of the code fits into the instruction cache?


### Size of structs and functions

Inspect a binary with `nm`:

	nm --print-size --radix d $BINARY

Filter the output for symbols stored in a data (struct) and text
(function) segments.


### Instruction locality

Check the call tree for functions called frequently. The full call tree looks
like this, but doesn't reflect specific conditions.

	saturate
	quantize_12bit
	app_render_audio
		sequencer_tick
			sequencer_state_play
				sampler_trigger
			sequencer_state_idle
			sequencer_advance
				frames_per_step
				step_index_update
		sampler_render_audio
			sampler_state_play
				interpolate_linear
					convert_int16_to_float
				interpolate_hold
					convert_int16_to_float
			sampler_state_idle

The most frequent call tree:

	saturate
	quantize_12bit
	app_render_audio
		sequencer_tick
			sequencer_state_play
			sequencer_advance
				frames_per_step
				step_index_update
		sampler_render_audio
			sampler_state_play
				interpolate_linear
					convert_int16_to_float

For each sample, the CPU branches to 10 different functions. Can they all
fit in the 1024-byte cache simultaneously?

	134220253 00000204 T sequencer_tick
	134219649 00000160 T interpolate_linear
	134219907 00000070 t sequencer_state_play
	134219809 00000052 T sampler_render_audio
	134219185 00000048 T app_render_audio
	134220489 00000040 T saturate
	134220457 00000032 T quantize_12bit
	134220529 00000024 T convert_int16_to_float

Some functions in the call tree don't appear in the output of `nm`. The
compiler inlined them.

These functions together take 630 bytes. They will fit well within the
cache, but there isn't much room for growth. We don't like this because
we haven't finished writing the application. However, the target CPU
will change, and might have a bigger instruction cache.

A cache line holds 16 bytes. Some of these don't take up a full cache line,
so will create fragmentation:

	134220253 00000204 T sequencer_tick         # 12.75 cache lines
	134219907 00000070 t sequencer_state_play   # 4.387 cache lines
	134219809 00000052 T sampler_render_audio   # 3.25 cache lines
	134220489 00000040 T saturate               # 2.5 cache lines
	134220529 00000024 T convert_int16_to_float # 1.5 cache lines

I remember a software synthesizer written in Go that did all the audio
processing one buffer at a time. Each processor had a method to fill a
buffer.  I thought the author only intended the buffering to amortize I/O
latency. I didn't realize that the buffering also reduces the number of
cache misses per buffer period, so even microcontrollers, a platform with
little I/O latency, would benefit from this design. It treats the i-cache well.

	void
	proc(struct proc *p, const float *input, float *output, size_t len)
	{
		for (uint32_t i = 0; i < len; i++) {
			output[i] = func(p, input[i]);
		}
	}

You can get one instruction cache miss per stage of processing per buffer
period. This allows `func` to occupy most of the instruction cache, so it
needn't share space with other stages. That allows more complex processing
in each stage.

You need space for the buffers. 96 floats is 384 bytes. We can allocate
it statically. Where would the linker put it? It put our output buffers
(int16_t) here:

	$ nm --print-size .build/cross/mcu/mcu-app | grep buffer
	20000a96 00000060 b buffer
	20000a36 00000060 b buffer1

b = BSS which RAM includes.

	.bss : {
		*(.bss*)	/* Read-write zero initialized data */
		*(COMMON)
		. = ALIGN(4);
		_ebss = .;
	} >ram

That's good: it'll be fast.


### Branch and cache misses

On Linux, we can measure branch and cache misses by using `perf`:

	perf record \
		--event=cache-misses \
		--event=branch-misses \
		--output=perf.data $BINARY


## Wall clock time

- Microcontroller: toggle a GPIO pin before and after a code block
- Linux: hardware timer

