# Sequencer

There are several ways to implement a sequencer.


## MIDI generator

One view of a sequencer is as a source of MIDI data. This implies that
the internal interface between the sequencer and the voice is MIDI.
The sequencer's internal data format may differ from MIDI, but it has
to be transformable into MIDI.

Midi has note-on and note-off messages. This allows it to support notes
of arbitrary length. Whereas, the sequencer might represent notes only
of finite duration.


## MIDI with extensions

MIDI is somewhat generic and might not be a good fit for the interface
between the sequencer and sampler voice. One drawback of MIDI is the
7-bit resolution of parameters.


## Array of steps

Each step is associated with a certain number of frames, which varies
as a function of tempo. The sequencer maintains two counters: the step
counter and the frame counter. The sequencer updates its state for each
frame. After the frame counter reaches the number of frames per step,
the frame counter is reset to zero and the step counter is incremented. If
the step counter reaches the end of the pattern, it's reset to zero.

If the frame counter is zero and the step is populated, then the step
is triggered. Otherwise, nothing happens.

It's possible to adapt this approach to other kinds of triggers, such as
changing parameters or turning notes off. However, the chief attribute of
the "array of steps" design is that the timing of events is inherently
quantized. If two different parameters are associated with the same
instant in time, they are encoded in the same step. Steps define the
set of instants during which events can occur.

Irregular sequences can be obtained by increasing the number of steps
per pattern or by calculating the frames per step based on step parity
and swing in addition to bpm and frame rate.


## List of notes

Each note is associated with a pitch and duration and sometimes
other stuff.

