Careful thought makes excellent code. Judge engineering practices by
how clearly they make you think. How can you choose among them?  See how
your situation narrows your options.

I code between parenting, spousing, and working. It's how I teach myself
new tools. It's my time to screw up, overdo, pretend, and play. My practices
fit into an hour of twiddling.

1. I simplify by avoiding POSIX and microcontroller features. Each
   platform has a directory for code that targets it. But most of the code
   targets _no_ platform. I shun preprocessor conditionals.

2. I track my assumptions by writing assertions. Some assumptions, such as
   that a pointer points to allocated memory, assertions can only approximate.
   But most we can assert exactly. Assertions double as a runtime check of
   correctness. If code requires a condition to run as intended, we assert
   that condition right before we need it.

3. I specify behavior in table-driven unit tests. Tables encourage
   consistency and organization.

4. I understand functions by writing them short and simple. I prefer functions
   that barely impact the environment. I specify them easily.

5. I find few bugs by running static and dynamic analyzers. Either I'm
   misusing them or my other practices suffice. I enjoy exploring them.

6. I record a project's history by writing clear commit messages. I summarize.
   I put what changed and why. A well-written commit message can stop a bad
   commit.

7. I use a strict, modern dialect of C. C encourages concreteness and
   discourages indirection. This makes code understandable.

8. I remove abandoned and unused code and writing.
