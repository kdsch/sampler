# Monophony

The sampler plays one sample at a time.
This sounds simple, but realizing it is complex.


## Original method

The first implementation uses two states: playing and stopped.
The sampler plays when the sequencer triggers it.
The sampler stops when the index reaches the end of the sample.
This works.
It has zero trigger delay.
However, when the sequencer triggered in the play state, it pops audibly.
Better sound quality would be nice.

Removing the pop under monophony implies a fadeout.
I thought of two ways to do it.


## New method 1: predictive fadeout

The sequencer can predict when a fadeout might be needed.
The sampler can have a new function to put the sampler in a fadeout state.

	void sampler_fadeout_trigger(struct sampler *s);

The sequencer can look ahead one fade length.
If there is a trigger at that time, the sequencer can call the fadeout trigger.


### Subproblem: computing fade length

The sampler measures time in frames.
The sequencer measures time in a much lower resolution: ticks.
If the fadeout time is in ticks, then the sequencer can compute fade length without error.
If the fadeout time can have any value in units of frames, then the sequencer's computation of fade length will have a nonzero error.

We have two options: define the fade time in ticks or tolerate the error.

#### Tolerating error

There are two possible errors: the sequencer under- or over-estimates the fade time.

If the sequencer overestimates the fade time, then it will trigger the fade earlier than necessary.
This might be acceptable, because the fade will hit zero before the next trigger.
There will be no pop.

Otherwise, the sequencer will trigger the fade too late to avoid the pop.
It would be silly to undertake all this to not solve the original problem.

Therefore, the sequencer ought to overestimate the fadetime.

## New method 2: reactive fadeout

I can make the fadeout short enough that users don't notice the trigger delay it causes.
When the sampler is triggered while playing, it can begin fading out.
At the end of the fadeout, it can play the next sample.
But there's another case: the sample can end before the end of the fadeout.
Then the sampler can play the next sample.

Realizing this correctly requires careful thought.

There are three states and a few events that cause transitions.

	      stopped
	        | ↑
	trigger | | end_of_sample
	        ↓ |
	      playing
	        | ↑
	trigger | | end_of_sample || end_of_fade
	        ↓ |
	      fadeout

The playing and fadeout states are similar.
Both have to update the index, output, and state variables.
But the fadeout state also has to update the sample and fade frame count variables.

	state    output  index  state  sample  fade_frame_count
	-------  ------  -----  -----  ------  ----------------
	stopped
	playing       x      x      x
	fadeout       x      x      x       x                 x

Playing and fadeout update the index in the same way.
Otherwise, they're different.
Playing uses linear interpolation to compute the output.
Whereas, fadeout uses linear interpolation with a time-varying gain.

Follow the form of the sequencer.
Write functions that update variables:

- `index_update`
- `output_update`
- `state_update`
- `sample_update`
- `fade_frame_count_update`

