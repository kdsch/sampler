# The architecture of a voice

## Feature idea

Support conventional subtractive synthesis and sampling.


## Implementation

Overall design:

	                _______
	[oscillator]-->|       |
	               | mixer |-->[filter]-->[amplifier]--+--> output
	+->[sampler]-->|_______|                           |
	|                                                  |
	+--------------------------------------------------+

Hardware design:

	                       _______
	             [vco]--->|       |
	                      |       |
	   ................   | mixer |-->[vcf]-->[vca]--+--> output
	   :  mcu         :   |       |                  |
	   :              :   |       |                  |
	+--:-->[sampler]--:-->|_______|                  |
	|  :              :                              |
	|  :..............:                              |
	+------------------------------------------------+

We can implement the signal processors other than the sampler either in
hardware or software. When targeting POSIX, for example, we'd implement
oscillators and filters in software. An MCU target could implement them
as software _or_ hardware. Therefore, we weill implement the software
signal processors in the pure tree. Hardware-based signal processors
will live in the MCU tree.

Obviously the hardware and software implementations have to use the
same interface. The fact that one depends on the MCU environment and
the other doesn't creates an interesting design problem.


Consider a VCO. It has several control voltage inputs and a set of audio
signal outputs. A dynamic software interface to an oscillator could look
like this:

	struct oscillator {
		void *impl;
		void (*set_freq)(void *impl, float freq);
		void (*set_duty)(void *impl, float freq);
		float (*output_square)(void *impl);
		float (*output_sawtooth)(void *impl);
		float (*output_triangle)(void *impl);
	};

We cannot use a tagged union in this case because the implementations
don't target the same platform. Or we can put these functions at the
module level.

	// osc.h
	void osc_set_freq(struct osc *osc, float freq);
	void osc_set_duty(struct osc *osc, float duty);
	float osc_output_square(struct osc *osc);
	float osc_output_sawtooth(struct osc *osc);
	float osc_output_triangle(struct osc *osc);

This interface doesn't define `struct osc` and therefore requires us to
define it before inclusion by an implementation.

The build system would determine which implementation to use by selecting
the set of source files.

We can apply this approach to any signal processor.


## Right approach, wrongly applied

We inaccurately assume, with this approach, that each hardware signal
processor interfaces with the MCU. We will choose analog routing and
mixing.  We need and interface at the level of a voice, not
a signal processor. We might find this design more appropriate:

	// voice.h
	void voice_osc_pitch(struct voice *v, float pitch);
	void voice_osc_interval(struct voice *v, float interval);
	void voice_osc1_shape(struct voice *v, float shape);
	void voice_osc2_shape(struct voice *v, float shape);

	void voice_sampler_speed(struct voice *v, float speed);
	void voice_sampler_sample(struct voice *v, uint32_t sample);
	void voice_sampler_start(struct voice *v, float start);
	void voice_sampler_length(struct voice *v, float length);
	void voice_sampler_speed_mod(struct voice *v, float amount);
	void voice_sampler_loop(struct voice *v, bool loop);
	void voice_sampler_loop_point(struct voice *v, float loop_point);

	void voice_mixer_osc1(struct voice *v, float level);
	void voice_mixer_osc2(struct voice *v, float level);
	void voice_mixer_sampler(struct voice *v, float level);
	void voice_mixer_ext(struct voice *v, float level);

	void voice_filter_freq(struct voice *v, float freq);
	void voice_filter_q(struct voice *v, float q);
	void voice_filter_tone(struct voice *v, float tone);
	void voice_filter_mod(struct voice *v, float amount);

	float voice_output(struct voice *v);

Essentially the interface lists parameters and outputs.

Note that this interface doesn't define `voice_init`. Should it? That
depends on whether the two implementations share that function.

In hardware, setting these parameters amounts to writing to PWM duty
cycle registers. For now, let's assume this rather than inject PWM as
a dependency.

Software will store these parameters in a map.

The POSIX and MCU builds share the sampler implementation.

pure/voice.c is the pure software implementation of a voice. All components
target no specific platform.

mcu/voice.c is the hardware implementation of a voice. It uses the pure
software sampler implementation (because hardware does not implement
it). We implement everything using MCU peripherals. It assumes a
specific board.

Both implementations include pure/voice.h and implement all the functions
it declares.

Because pure/voice.c targets no platform, the build system can choose three
different configurations:

- MCU with pure/voice.c
- MCU with mcu/voice.c
- POSIX with pure/voice.c

I see little point in having  a posix/voice.c that implements a voice using
POSIX primitives.

Defining a module interface as pure allows us to implement it on any platform.

