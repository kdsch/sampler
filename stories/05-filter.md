# Filter

Classic analog filters have nonlinearities that give them a unique tone
and permit stable self-oscillation.


## Analysis

Engineers can accurately model analog filters with nonlinear differential
equations. We find analyzing and modeling these equations difficult,
but our intuition helps.

We can model some nonlinearities as state-dependent parameter changes.

We believe a filter with a warbly tone allows its state to modulate
its cutoff frequency. The input signal modulates the cutoff frequency,
making FM sidebands. We can hear this (and see it in a spectrogram) with
lower Q settings as an irregularity in the transition band. We guess this
gives analog filters their "warmth," "creaminess," or "thickness." When
a filter sounds cold, we guess that it produces inaudible sidebands,
so the filter sounds clean and linear.

We believe that a self-oscillating filter must reduce its Q as the
amplitude of the bandpass state increases.

This makes sense. Consider the case of a state-variable filter based on
the KHN topology using OTAs for controlling cutoff frequency. OTAs have
a nonlinear behavior: they start to lose gain as the input amplitude
increases, effectively reducing the rate of integration and therefore
the cutoff frequency.


## Implementation

### Analog

We find it easy to make a nonlinear analog filer. Pick a filter topology
and prototype it. Tune the component values for the tone you want.


### Digital

We find it hard to model a nonlinear analog filter in the digital domain. 