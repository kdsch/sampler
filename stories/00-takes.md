# Takes

## Feature idea

A take transforms a note in a sequence into a concretized version of itself.
It converts a note that we define in terms of parameters into a note which
we define in terms of a sample.

Whether these two things differ depends on implementation details.
If the application samples a voice that itself can  play back samples, then
a take results in a note with parameters that play back the taken sample.

As a basic example, consider a voice with this architecture:

	                _______
	[oscillator]-->|       |
	               | mixer |-->[filter]-->
	   [sampler]-->|_______|

This diagram doesn't show parameters like frequency or volume. We
impute parameters of components to the voice. We assume a design that
can at least play back samples.

This design can function as both a subtractive synthesizer and a sample
player.

A take outputs a parameter map that causes a sample to play back. In
the above example, we might have:

- oscillator volume: 0
- sampler volume: 1
- sampler sample index: n+1
- filter frequency: max
- filter tone: lowpass
- filter q: 0.707

This means that the `take` function has to know about the voice
architecture.  We probably won't add sufficient abstractions around
voices make this unnecessary.

If we implement the oscillator in hardware, then we can choose only
the dual-source architecture in the diagram above.

Or we can allow polymorphic voices. However, this feature aims to
reduce the friction from "I want to freeze this note and apply some more
processing" to actually doing it. Therefore it makes sense to have only
one voice architecture. If we don't expect to need to mix oscillators
and samples, we could implement that part of the chain with polymorphism.

Anyway, a take produces a sample.

A take also changes the note in the sequence. The sequence contains notes.
Each note has a parameter map. When the sequencer plays, it hands triggered
notes to the sequenced voice, which plays the appropriate sound.

The application executes a take with these arguments:

- a mutable sequence
- a note in that sequence
- an audio stream to sample (source)
- a mutable datastore (for storing the sample)

With a stopped transport, the take triggers a note immediately. With a
playing transport, the take happens upon the next trigger of that note.

## Code

What could this look like in code? C makes it hard to work abstractly, so
take this as provisional.

The take has some state, as the application interleaves its operation
with signal processing. Taking happens after the application updates
the input signal.

	struct take {
		struct sequence *sequence;
		uint32_t note_index;
		const float *input;
		struct datastore *data;
		struct buffer buf;
		void (*state_func)(struct take *t);
	};

Takes follow the basic paradigm of having a "update" method that the
application calls for each frame.

	void
	take_render_audio(struct take *t)
	{
		assert(t);
		assert(t->state_func);
		t->state_func(t);
	}

The application triggers the take.

	void
	take_trigger(struct take *t)
	{
		assert(t);
		t->state_func = take_record;
	}

When the application executes a take, updating means recording the note
into a buffer. If the buffer fills up to the length of the note, then the take
stops: the sample is added to the datastore and injected into the sequence.

	void
	take_record(struct take *t)
	{
		if (t->buf->len < samples_from_beats(t->note->len)) {
			assert(t->input);
			buffer_append(t->buf, *t->input);
			return;
		}

		const struct sample *take = datastore_add(t->data, t->buffer);
		struct note n = sequence_note(t->sequence, t->note_index);
		n.sample = take;
		sequence_insert(t->sequence, t->note_index, n);
		t->state_func = take_idle;
	}

Note that `samples_from_beats` needs to know the sample rate and
bpm. Instead of that, it's also possible to pre-calculate the length
of the take. But generally we don't know that in advance. Consider
situations where something changes the tempo during playback. We can
more robustly calculate the length of the take by having a "tail"
parameter that counts a certain number of samples after the end of the
note (for a "timeout") coupled with a level detector threshold. We could
also let the user manually punch out somehow.

When idling, the take does nothing.

	void
	take_idle(struct take *t)
	{
		(void)t;
	}

## UI/UX

A simple UI idea: have the typical 16-button step sequencer with another
button called "TAKE". Hold TAKE and press a step to resample that step.

	[0] [1] [2] [3]
	[4] [5] [6] [7]
	[8] [9] [A] [B]
	[C] [D] [E] [F]

	[TAKE]

You don't have
to arm recording or playback at just the right spot. There's less cleanup:
you select more accurately the sound you want to sample, so there is less
need to trim the result. It gives you precision and ease in specifying
what to record.

An instrument that has a button to trigger notes can source a take. It's
possible to take and tweak knobs simultaneously.

If you press and hold take for a second, you can release it to arm TAKE.
That allows you to use two hands to manipulate controls as the recording
happens.

## Persistence

Users might like it if doing a take didn't _permanently_ delete the previous
step. It might contain useful information. Maybe you decide
later to redo the take with different parameters. So it makes
sense for us to define a sequence as a persistent data structure.


## Keyboard takes

If the instrument has a keyboard, the user might enjoy taking a key. This
has a meaning similar to taking notes in a sequence. But the key doesn't
have a parameter map or a duration.

The Novation Bass Station 2's AFX mode inspires this. The user
can assign each key to a different patch. The Korg microsampler also
allows you to associate each key with a different sample.

A keyboard resembles a sequence.

The instrument doesn't _need_ a keyboard to have this feature. It just needs
the concept of a keymap, which maps MIDI notes to samples/parameter maps.

