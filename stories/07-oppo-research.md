I don't really view existing samplers as opposition, but if this
research has any hope of becoming an actual product some day, other
samples should inform the way it works.

# digitakt

> Digitakt is a very competent and accessible sampler. It can sample
audio from external sources via the external inputs, and it can also
sample audio internally from the Digitakt itself.

## start sampling immediately

0. Access the sampling menu by pressing [SAMPLING].
1. Start sampling immediately by pressing [FUNC] + [YES].
2. Stop by pressing [YES].


## arm sampling for threshold trigger

0. Access the sampling menu by pressing [SAMPLING].
1. Press [YES] to arm the sampler.
2. Press [SAMPLING] + [NO] to cancel.


# op-1 field

> the synth sampler is a chromatic stereo sampler with region loop
> functionality. playing the keyboard will play the sound from the start
> point, loop a section if enabled and play it through to the end upon
> release.
> 
> you can import your own sounds or sample straight into your OP-1 field
> using any input source.
> 
> to sample a sound using the built-in microphone, make sure the sampler
> is selected and press the input key (the top right key with a mic symbol).
> choose microphone as input.
> 
> adjust threshold and gain settings (gray and orange). hold any key and
> speak into the microphone. release the key and then play the keyboard.
> 
> use the encoders to trim the start and end points of your sound and enjoy
> the beautiful sound of your voice.


# po-33

> hold record + any key from 1-16. the microphone will record until the keys
are released, and the recording will be stored in the position you selected
(1-16). if a cable is inserted into the line in jack, the PO-33 will record
via line in.

# es-1

You can record samples in sample, pattern, or song mode.

## start sampling immediately

1. Enter sample mode by pressing the Sample mode key.
2. Hold Pattern Set and press Rec to enter sampling-ready mode.
3. Select either stereo or mono by turning the dial.
4. Press Play to begin sampling immediately.
5. Press Stop to end sampling.
6. Save the sample by pressing Write.

## resampling

1. Enter pattern or song mode.
2. Press Stop to stop playback.
3. Hold Pattern Set and press Rec to enter sampling-ready mode.
4. Select either stereo or mono by turning the dial.
5. Press Play to begin sampling.
6. Press Stop to end sampling.

## punch in during playback

1. Hold Pattern Set and press Rec to begin sampling immediately.
2. End sampling by pressing Rec (to continue playback) or Stop (to stop
   playback).

## resampling one note of a part

0. Enter pattern mode.
1. Ensure the transport is stopped.
2. Hold Pattern Set and press Rec to arm sampling.
3. Press a part key. This will trigger the part and begin sampling.
4. Press Stop to end sampling.


# microsampler

> The microSAMPLER is a sampling keyboard that uses the keyboard to
sample and play back. By switching the function of the keyboard, you
can assign various different samples to the keyboard, or you can assign
a single sample to be played by the 37-note keyboard at different pitches.

> You can choose from five different sampling methods to suit your needs.
For example, KEY GATE lets you record multiple samples consecutively by
using the keys as REC switches. AUTO NEXT lets you automatically sample
onto multiple keys one after the other.

> You can connect the microSAMPLER to your computer via USB, and use the
microSAMPLER's dedicated editor/librarian software to conveniently manage
samples or edit parameters.


