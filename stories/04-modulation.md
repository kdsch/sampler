# Modulation

When designing a sound, I like having control over extremes.

An envelope can be a unipolar modulation source. Some synths have an
envelope intensity control that can go negative to create an inverted
envelope. Negative envelopes sometimes require the user to adjust
the offset.

I find a fixed offset useful. I will make it the default for both LFOs
and envelopes.

Let's illustrate this with a diagram. A positive ADSR looks like this:

	1      -
	      / \
	a    /   ---
	    /       \
	0 --         --

A "negative" adsr looks like this:

	1 --         --
	    \       /
	     \   ---
	      \ /
	0      -

It doesn't actually go negative, it just flips upside-down within the
same range as the positive one.

We can do the same thing with LFOs. Don't use bipolar LFOs. The positive:

	1     -       -
	     / \     /
	    /   \   /
	   /     \ /
	0 -       -

Negative:

	1 -       -
	   \     / \
	    \   /   \
	     \ /     \
	0     -       -