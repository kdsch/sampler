oscillator 0:
	inputs:
		gate
		frequency
		pulse width
		gate sync
	outputs:
		square
		triangle
		sawtooth
		sync out
oscillator 1:
	inputs:
		gate
		frequency
		pulse width
		gate sync
		sync in
	outputs:
		square
		triangle
		sawtooth
sampler:
	inputs:
		gate
		audio in
		sample name
		speed
		loop
		length
		start point
		direction
		interpolator
	outputs:
		audio out
filter:
	inputs:
		audio in
		frequency
		q
	outputs:
		lowpass
		bandpass
		highpass
envelope:
	inputs:
		gate
		delay
		attack
		delay
		decay
		sustain
		release
	outputs:
		envelope out
lfo:
	inputs:
		gate
		frequency
		waveform
		gate sync
	outputs:
		lfo out
