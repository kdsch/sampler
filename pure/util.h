#ifndef UTIL_H
#define UTIL_H
#include <stdint.h>
/// quantize_12bit maps x ∈ [-1, 1] to a return value ∈ [0, 0xFFF].
uint16_t quantize_12bit(float x);
float saturate(float x);
uint32_t xorshift32(uint32_t state);
#endif
