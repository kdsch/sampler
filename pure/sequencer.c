#include "sequencer.h"
#include "len.h"
#include "samples.h"
#include <assert.h>

static const struct trigger triggers[] = {
	{
		.sample = &samples[0], // bass
		.speed = 1.0,
		.start_point = 0.33f,
		.smoothing = 1,
		.gain = 1,
	},
	{
		.sample = &samples[1], // kick
		.speed = 1.25,
		.start_point = 0,
		.smoothing = 1,
		.gain = 1,
	},
	{
		.sample = &samples[2], // snare
		.speed = 0.75,
		.start_point = 0,
		.smoothing = 0,
		.gain = 1,
	},
	{
		.sample = &samples[3], // hat
		.speed = 0.75,
		.start_point = 0,
		.smoothing = 0,
		.gain = 0.5,
	},
};

static float
ticks_per_step(float tick_rate, uint16_t bpm_x10)
{
	assert(0 < tick_rate);
	assert(0 < bpm_x10);

	// f_t ticks  60 s   min   1 beat
	// ---------- ---- ------- -------
	//     s      min  B beats 4 steps
	return tick_rate * 60 * 10.f / (bpm_x10 * 4);
}

static uint16_t
tick_index_update(uint16_t index, float ticks_per_step)
{
	assert(index < UINT16_MAX);
	assert(0 < ticks_per_step);
	assert(ticks_per_step <= UINT16_MAX);

	const uint16_t next = index + 1;

	if (ticks_per_step <= next) {
		return 0;
	}

	assert(next <= ticks_per_step);

	return next;
}

static uint8_t
step_index_update(uint8_t index, uint8_t len)
{
	assert(0 < len);
	return (uint8_t)(index + 1) % len;
}

static void
sequencer_advance(struct sequencer *s)
{
	assert(s);
	s->tick_index = tick_index_update(s->tick_index, ticks_per_step(s->tick_rate, s->bpm_x10));

	if (s->tick_index == 0) {
		s->step_index = step_index_update(s->step_index, len(s->steps));
	}
}

void
sequencer_state_set(struct sequencer *s, enum transport_state state)
{
	assert(s != NULL);

	if (s->state == state) {
		return;
	}

	if (state == TRANSPORT_STATE_PLAYING) {
		assert(s->state == TRANSPORT_STATE_STOPPED);
		s->tick_index = 0;
		s->step_index = 0;
		s->state = TRANSPORT_STATE_PLAYING;
		return;
	}

	assert(state == TRANSPORT_STATE_STOPPED);
	s->state = TRANSPORT_STATE_STOPPED;
}

static void
sequencer_step_set(struct sequencer *s, uint8_t step_index, uint8_t trigger_index)
{
	assert(s);
	assert(step_index < len(s->steps));
	assert(trigger_index < len(triggers));
	s->steps[step_index] = &triggers[trigger_index];
}

static void
sequencer_step_clear(struct sequencer *s, uint8_t step_index)
{
	assert(s);
	assert(step_index < len(s->steps));
	s->steps[step_index] = NULL;
}

static void
sequencer_steps_default_pattern(struct sequencer *s)
{

	for (uint8_t i = 0; i < len(s->steps); i++) {
		sequencer_step_clear(s, i);
	}

	struct {
		uint8_t step_index;
		uint8_t trigger_index;
	} const pattern[] = {
		{0, 1},
		{2, 3},
		{4, 2},
		{6, 0},
		{10, 1},
		{12, 2},
		{15, 3},
	};

	assert(len(pattern) <= len(s->steps));

	for (uint8_t i = 0; i < len(pattern); i++) {
		sequencer_step_set(s, pattern[i].step_index, pattern[i].trigger_index);
	}
}

void
sequencer_init(struct sequencer *s, float tick_rate)
{
	assert(s);
	s->state = TRANSPORT_STATE_STOPPED;
	s->bpm_x10 = 1800;
	s->tick_index = 0;
	s->step_index = 0;
	sequencer_steps_default_pattern(s);
	s->tick_rate = tick_rate;
}

const struct trigger *
sequencer_tick(struct sequencer *s)
{
	assert(s);

	if (s->state != TRANSPORT_STATE_PLAYING) {
		assert(s->state == TRANSPORT_STATE_STOPPED);
		return NULL;
	}

	assert(s->step_index < len(s->steps));
	const struct trigger *trigger =
		s->tick_index == 0 ? s->steps[s->step_index] : 0;

	sequencer_advance(s);
	return trigger;
}
