#include "sampler.h"
#include "sample.h"
#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>

void
sampler_init(struct sampler *s, float frame_rate)
{
	assert(s != NULL);

	s->frame_rate = frame_rate;
	s->trigger = NULL;
	s->index = 0;
}

/// index_from_start_point : start ∈ [0, 1] → [0, len - 1]
static float
index_from_start_point(size_t len, float start)
{
	assert(0 <= start);
	assert(start <= 1);
	assert(len <= 1 << FLT_MANT_DIG);

	return start * (float)(len - 1);
}

void
sampler_trigger(struct sampler *s, const struct trigger *t)
{
	assert(s != NULL);

	if (t == NULL) {
		return;
	}

	assert(t->sample != NULL);

	s->index = index_from_start_point(t->sample->len, t->start_point);
	s->trigger = t;
}

static bool
sample_index_in_range(size_t len, float index)
{
	assert(len <= 1 << FLT_MANT_DIG);

	// Linear interpolation indexes two adjacent frames,
	// which is why we compare to one less than the maximum
	// frame index.
	return 0 <= index && index < len - 1;
}

static float
float_from_int16(int16_t x)
{
	const float result = (float)x / (INT16_MAX + 1);

	assert(fabsf(result) <= 1);

	return result;
}

static float
interpolate(const struct sample *sample, float index, float smoothing)
{
	assert(sample != NULL);
	assert(sample->frames != NULL);
	assert(sample_index_in_range(sample->len, index));

	const uint32_t lower = (uint32_t)index;
	const float delta = index - (float)lower;

	assert(0 <= delta);
	assert(delta <= 1);

	const float l = float_from_int16(sample->frames[lower]);
	const float u = float_from_int16(sample->frames[lower + 1]);

	assert(0 <= smoothing);
	assert(smoothing <= 1);
	const float result = l + smoothing * delta * (u - l);

	assert(fabsf(result) <= 1);

	return result;
}

static float
sampler_playing(struct sampler *s)
{
	assert(s != NULL);

	const struct trigger *const t = s->trigger;

	assert(t != NULL);
	assert(40 <= s->frame_rate);
	assert(s->frame_rate <= 96e3);
	assert(t->sample != NULL);
	assert(40 <= t->sample->frame_rate);
	assert(t->sample->frame_rate <= 96e3);
	assert(fabsf(t->speed) <= 1e3);
	assert(sample_index_in_range(t->sample->len, s->index));

	const float delta = t->speed * t->sample->frame_rate / s->frame_rate;

	assert(fabsf(delta) <= 1e3 * 96e3 / 40);

	const float index_new = s->index + delta;

	assert(index_new <= (1 << FLT_MANT_DIG) + 1e3 * 96e3 / 40);

	if (!sample_index_in_range(t->sample->len, index_new)) {
		s->trigger = NULL;
		return 0;
	}

	assert(fabsf(t->gain) <= 1e2);
	assert(1e-2 <= fabsf(t->gain));

	const float result = t->gain * interpolate(t->sample, index_new, t->smoothing);

	assert(fabsf(result) <= t->gain);

	s->index = index_new;
	return result;
}

float
sampler_render_audio(struct sampler *s)
{
	assert(s);

	return s->trigger ? sampler_playing(s) : 0;
}
