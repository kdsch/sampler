#ifndef LEN_H
#define LEN_H
/// len(x) returns the number of members of a static array.
#define len(x) (sizeof(x) / sizeof(x[0]))
#endif
