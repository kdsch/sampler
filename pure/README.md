# pure/

This library uses only the subset of C code that has the same behavior on
POSIX as it does on a microcontroller.

We find it tricky to define this subset. Here's what we know so far:

1. It excludes microcontroller-specific features like
   memory-mapped peripherals and interrupts because POSIX doesn't have them.

2. It excludes assembly language because platforms have different ISAs.

3. It excludes C standard library functions that might use system
   calls because microcontrollers don't have system calls.

4. It declares no mutable global variables because we want to
   test it easily.

5. It includes <stdint.h> types. It doesn't use C's built-in
   integer types (`short`, `int`, `long`) because their size is unclear.

If the library needs to use stuff that depends on a platform, it should
define a struct of function pointers for that. The microcontroller and
POSIX platforms would provide functions.
