#ifndef TRIGGER_H
#define TRIGGER_H
/// A trigger associates a sample with a set of playback parameters.
struct trigger {
	const struct sample *sample;
	float speed;
	float start_point;
	float smoothing;
	float gain;
};
#endif
