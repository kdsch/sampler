#ifndef BUFFERS_H
#define BUFFERS_H
#include <stddef.h>
#include <stdint.h>

/// A sample associates a frame rate with an array of 16-bit integer frames.
struct sample {
	float frame_rate;
	size_t len;
	const int16_t *frames;
};
#endif
