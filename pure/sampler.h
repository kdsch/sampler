#ifndef SAMPLER2_H
#define SAMPLER2_H
#include "trigger.h"
/// A sampler plays back audio samples according to parameters in a trigger.
struct sampler {
	const struct trigger *trigger;
	float frame_rate;
	float index;
};

/// sampler_init initializes a sampler with the given frame rate.
void sampler_init(struct sampler *s, float frame_rate);

/// sampler_trigger sets the sampler to play back using the trigger parameters.
void sampler_trigger(struct sampler *s, const struct trigger *t);

/// sampler_render_audio returns the next frame of the audio stream.
float sampler_render_audio(struct sampler *s);
#endif
