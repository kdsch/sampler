#ifndef BITSET_H
#define BITSET_H
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>

/// bitset32 provides a lock-free 32-bit bitset.
struct bitset32 {
	atomic_uint_least32_t value;
};

/// bitset32_init initializes the bitset.
void bitset32_init(struct bitset32 *bitset);

/// bitset32_set sets the bit of the given index.
void bitset32_set(struct bitset32 *bitset, uint8_t bit_index);

/// bitset32_clear clears the bit of the given index.
void bitset32_clear(struct bitset32 *bitset, uint8_t bit_index);

/// bitset32_get returns the bit of the given index.
bool bitset32_get(const struct bitset32 *bitset, uint8_t bit_index);
#endif
