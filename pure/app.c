#include "app.h"
#include "len.h"
#include "samples.h"
#include "util.h"
#include <assert.h>

void
app_init(struct app *app, float frame_rate, uint8_t frames_per_tick)
{
	assert(app);
	app->frames_per_tick = frames_per_tick;
	app->frame_index = 0;

	sampler_init(&app->sampler, frame_rate);

	assert(0 < frames_per_tick);
	sequencer_init(&app->sequencer, frame_rate / frames_per_tick);
}

float
app_render_audio(struct app *app)
{
	assert(app);

	if (app->frame_index == 0) {
		sampler_trigger(&app->sampler, sequencer_tick(&app->sequencer));
	}

	const float result = sampler_render_audio(&app->sampler);

	assert(app->frame_index <= app->frames_per_tick);

	app->frame_index = app->frame_index < app->frames_per_tick ? app->frame_index + 1 : 0;
	return result;
}

void
app_transport_state_set(struct app *app, enum transport_state state)
{
	sequencer_state_set(&app->sequencer, state);
}
