#include "bitset.h"
#include <assert.h>

void
bitset32_init(struct bitset32 *bitset)
{
	assert(bitset);
	assert(atomic_is_lock_free(&bitset->value));

	atomic_init(&bitset->value, 0);
}

void
bitset32_set(struct bitset32 *bitset, uint8_t bit_index)
{
	assert(bitset);
	assert(bit_index < 32);

	atomic_fetch_or(&bitset->value, 1u << bit_index);
}

void
bitset32_clear(struct bitset32 *bitset, uint8_t bit_index)
{
	assert(bitset);
	assert(bit_index < 32);

	atomic_fetch_and(&bitset->value, ~(1u << bit_index));
}

bool
bitset32_get(const struct bitset32 *bitset, uint8_t bit_index)
{
	assert(bitset);
	assert(bit_index < 32);

	const bool result = !!(atomic_load(&bitset->value) & (1u << bit_index));

	assert(result == true || result == false);

	return result;
}
