#ifndef APP_H
#define APP_H
#include "sampler.h"
#include "sequencer.h"

/// An app implements a sampler application.
struct app {
	uint8_t frames_per_tick;
	uint8_t frame_index;
	struct sampler sampler;
	struct sequencer sequencer;
};

/// app_init initializes an app with the given frame rate and frames per tick.
void app_init(struct app *app, float frame_rate, uint8_t frames_per_tick);

/// app_render_audio renders one frame of audio.
float app_render_audio(struct app *app);

/// app_transport_state_set sets the state of the transport.
void app_transport_state_set(struct app *app, enum transport_state state);
#endif
