#include "bitset.c"
#include "sampler.c"
#include "sequencer.c"
#include "util.c"
#include <assert.h>
#include <math.h>

static void
harness_quantize_12bit_monotonic_SLOW(void)
{
	// quantize_12bit being monotonic is important for sound quality.
	float x, y;
	__CPROVER_assume(fabsf(x) <= 1);
	__CPROVER_assume(fabsf(y) <= 1);
	__CPROVER_assume(x < y);
	assert(quantize_12bit(x) <= quantize_12bit(y));
}

static void
harness_quantize_12bit_points(void)
{
	assert(quantize_12bit(-1) == 0);
	assert(quantize_12bit(0) == 0xFFF / 2);
	assert(quantize_12bit(1) == 0xFFF);
}

static void
harness_saturate(void)
{
	float x;
	__CPROVER_assume(!isnan(x));
	float result = saturate(x);
	assert(1 < x && result == 1 || x < -1 && result == -1 || x == result);
}

static void
harness_xorshift32_injective(void)
{
	uint32_t x, y;
	__CPROVER_assume(0 < x);
	__CPROVER_assume(0 < y);
	__CPROVER_assume(xorshift32(x) == xorshift32(y));
	assert(x == y);
}

static void
harness_float_from_int16_injective(void)
{
	int16_t x, y;
	__CPROVER_assume(float_from_int16(x) == float_from_int16(y));
	assert(x == y);
}

static void
harness_float_from_int16_approx_linear_SLOW(void)
{
	int16_t x, y;
	const bool overflow = (0 < y && INT16_MAX - y < x) || (y < 0 && x < INT16_MIN - y);
	__CPROVER_assume(!overflow);
	assert(fabsf(float_from_int16(x) + float_from_int16(y) - float_from_int16(x + y)) <= 2e-5);
}

static void
harness_float_from_int16_monotonic(void)
{
	int16_t x, y;

	__CPROVER_assume(x < y);
	assert(float_from_int16(x) <= float_from_int16(y));
}

static void
harness_float_from_int16_points(void)
{
	assert(0 == float_from_int16(0));
	assert(-1.0f == float_from_int16(INT16_MIN));
}

static void
harness_bitset(void)
{
	struct bitset32 bitset;
	bitset32_init(&bitset);
	uint8_t bit_index;
	__CPROVER_assume(bit_index < 32);
	bitset32_set(&bitset, bit_index);

	for (uint8_t i = 0; i < 32; i++) {
		if (i == bit_index) {
			assert(bitset32_get(&bitset, i));
		} else {
			assert(!bitset32_get(&bitset, i));
		}
	}

	bitset32_clear(&bitset, bit_index);

	for (uint8_t i = 0; i < 32; i++) {
		assert(!bitset32_get(&bitset, i));
	}
}

static void
harness_tick_index_update(void)
{
	float ticks_per_step;
	uint16_t index;
	__CPROVER_assume(index < UINT16_MAX);
	__CPROVER_assume(0 < ticks_per_step);
	__CPROVER_assume(ticks_per_step <= UINT16_MAX);
	tick_index_update(index, ticks_per_step);
}

int
main(void)
{
	// harness_quantize_12bit_monotonic_SLOW();
	harness_quantize_12bit_points();
	harness_saturate();
	harness_xorshift32_injective();
	harness_float_from_int16_injective();
	// harness_float_from_int16_approx_linear_SLOW();
	harness_float_from_int16_monotonic();
	harness_float_from_int16_points();
	harness_bitset();
	harness_tick_index_update();
}
