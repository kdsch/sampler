#ifndef SEQUENCER_H
#define SEQUENCER_H
#include "transport_state.h"
#include "trigger.h"
#include <stddef.h>
#include <stdint.h>

/// A sequencer generates audio by triggering a voice with a pattern of steps.
struct sequencer {
	enum transport_state state;
	uint16_t bpm_x10;
	uint16_t tick_index;
	uint8_t step_index;
	const struct trigger *steps[16];
	float tick_rate;
};

void sequencer_init(struct sequencer *s, float tick_rate);
const struct trigger *sequencer_tick(struct sequencer *s);
void sequencer_state_set(struct sequencer *s, enum transport_state state);
#endif
