#include "util.h"
#include <assert.h>

// quantize_12bit : [-1, 1] → [0, 2¹² - 1]
uint16_t
quantize_12bit(float x)
{
	assert(x <= 1);
	assert(-1 <= x);

	const float uint12_max = (1 << 12) - 1;
	const float scale_factor = uint12_max * 0.5f;
	const uint16_t result = (uint16_t)(scale_factor * (x + 1));

	assert(result <= 0xFFF);

	return result;
}

float
saturate(float x)
{
	if (1 < x) {
		return 1;
	} else if (x < -1) {
		return -1;
	}

	return x;
}

uint32_t
xorshift32(uint32_t x)
{
	assert(0 < x);

	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;

	assert(0 < x);

	return x;
}
