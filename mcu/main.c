/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Ken Sarkies <ksarkies@internode.on.net>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"
#include "bitset.h"
#include "len.h"
#include "util.h"
#include <assert.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/dac.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <stddef.h>

#define EVENT_DAC_TRANSFER_COMPLETE (1 << 0)
#define EVENT_BUTTON_PUSHED (1 << 1)

static uint16_t audio_buffers[2][48];
static struct bitset32 events;
static struct app app;

static void
clock_setup(void)
{
	rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);
}

static void
gpio_setup(void)
{
	// Port A and C are on AHB1
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOC);
	// Set the digital test output on PC1
	gpio_mode_setup(GPIOA, GPIO_MODE_INPUT, GPIO_PUPD_PULLDOWN, GPIO0);
	gpio_mode_setup(GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO1);
	gpio_set_output_options(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO1);
	// Set PA4 for DAC channel 1 to analogue, ignoring drive mode.
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO4);
}

static void
indicator_setup(void)
{
	rcc_periph_clock_enable(RCC_GPIOD);

	// Red
	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO14);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO14);

	// Green
	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO12);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO12);
}

static void
indicator_stopped(void)
{
	gpio_clear(GPIOD, GPIO12);
	gpio_set(GPIOD, GPIO14);
}

static void
indicator_playing(void)
{
	gpio_clear(GPIOD, GPIO14);
	gpio_set(GPIOD, GPIO12);
}

static void
timer_setup(uint32_t timer_period)
{
	// Enable TIM2 clock.
	rcc_periph_clock_enable(RCC_TIM2);
	rcc_periph_reset_pulse(RST_TIM2);
	// Timer global mode: No divider, Alignment edge, Direction up
	timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
	timer_continuous_mode(TIM2);
	timer_set_period(TIM2, timer_period);
	timer_disable_oc_output(TIM2, TIM_OC2 | TIM_OC3 | TIM_OC4);
	timer_enable_oc_output(TIM2, TIM_OC1);
	timer_disable_oc_clear(TIM2, TIM_OC1);
	timer_disable_oc_preload(TIM2, TIM_OC1);
	timer_set_oc_slow_mode(TIM2, TIM_OC1);
	timer_set_oc_mode(TIM2, TIM_OC1, TIM_OCM_TOGGLE);
	timer_set_oc_value(TIM2, TIM_OC1, 500);
	timer_disable_preload(TIM2);
	// Set the timer trigger output (for the DAC) to the channel 1 output
	// compare
	timer_set_master_mode(TIM2, TIM_CR2_MMS_COMPARE_OC1REF);
	timer_enable_counter(TIM2);
}

static void
exti_setup(void)
{
	nvic_enable_irq(NVIC_EXTI0_IRQ);
	exti_select_source(EXTI0, GPIOA);
	exti_set_trigger(EXTI0, EXTI_TRIGGER_RISING);
	exti_enable_request(EXTI0);
}

static void
dma_setup(void)
{
	// DAC channel 1 uses DMA controller 1 Stream 5 Channel 7.
	// Enable DMA1 clock and IRQ
	rcc_periph_clock_enable(RCC_DMA1);
	nvic_enable_irq(NVIC_DMA1_STREAM5_IRQ);
	dma_stream_reset(DMA1, DMA_STREAM5);
	dma_set_priority(DMA1, DMA_STREAM5, DMA_SxCR_PL_LOW);
	dma_set_memory_size(DMA1, DMA_STREAM5, DMA_SxCR_MSIZE_16BIT);
	dma_set_peripheral_size(DMA1, DMA_STREAM5, DMA_SxCR_PSIZE_16BIT);
	dma_enable_memory_increment_mode(DMA1, DMA_STREAM5);
	dma_enable_double_buffer_mode(DMA1, DMA_STREAM5);
	dma_set_transfer_mode(DMA1, DMA_STREAM5, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
	// The DMA stream targets the DAC1 12-bit right justified data register.
	dma_set_peripheral_address(DMA1, DMA_STREAM5, (uint32_t)&DAC_DHR12R1(DAC1));
	// The buffer array holds audio frames to be converted.
	dma_set_memory_address(DMA1, DMA_STREAM5, (uint32_t)audio_buffers[0]);
	dma_set_memory_address_1(DMA1, DMA_STREAM5, (uint32_t)audio_buffers[1]);
	dma_set_initial_target(DMA1, DMA_STREAM5, 0);
	dma_set_number_of_data(DMA1, DMA_STREAM5, len(audio_buffers[0]));
	dma_enable_transfer_complete_interrupt(DMA1, DMA_STREAM5);
	dma_channel_select(DMA1, DMA_STREAM5, DMA_SxCR_CHSEL_7);
	dma_enable_stream(DMA1, DMA_STREAM5);
}

static void
dac_setup(void)
{
	// Enable the DAC clock on APB1
	rcc_periph_clock_enable(RCC_DAC);
	// Set up DAC channel 1 with timer 2 as trigger source.
	// Assume the DAC has woken up by the time the first transfer occurs.
	dac_trigger_enable(DAC1, DAC_CHANNEL1);
	dac_set_trigger_source(DAC1, DAC_CR_TSEL1_T2);
	dac_dma_enable(DAC1, DAC_CHANNEL1);
	dac_enable(DAC1, DAC_CHANNEL1);
}

void
exti0_isr(void)
{
	exti_reset_request(EXTI0);
	bitset32_set(&events, EVENT_BUTTON_PUSHED);
}

void
dma1_stream5_isr(void)
{
	if (!dma_get_interrupt_flag(DMA1, DMA_STREAM5, DMA_TCIF)) {
		return;
	}

	dma_clear_interrupt_flags(DMA1, DMA_STREAM5, DMA_TCIF);
	bitset32_set(&events, EVENT_DAC_TRANSFER_COMPLETE);
}

static uint8_t
index_of_idle_audio_buffer(void)
{
	if (dma_get_target(DMA1, DMA_STREAM5)) {
		return 0;
	}

	return 1;
}

static void
platform_init(uint32_t timer_period)
{
	bitset32_init(&events);
	clock_setup();
	gpio_setup();
	indicator_setup();
	timer_setup(timer_period);
	dma_setup();
	dac_setup();
	exti_setup();
}

_Noreturn static void
event_loop(void)
{
top:
	if (bitset32_get(&events, EVENT_DAC_TRANSFER_COMPLETE)) {
		bitset32_clear(&events, EVENT_DAC_TRANSFER_COMPLETE);
		uint16_t *const buffer = audio_buffers[index_of_idle_audio_buffer()];
		gpio_set(GPIOC, GPIO1);

		for (uint8_t i = 0; i < len(audio_buffers[0]); i++) {
			buffer[i] = quantize_12bit(saturate(app_render_audio(&app)));
		}

		gpio_clear(GPIOC, GPIO1);
	}

	if (bitset32_get(&events, EVENT_BUTTON_PUSHED)) {
		bitset32_clear(&events, EVENT_BUTTON_PUSHED);

		if (app.sequencer.state == TRANSPORT_STATE_PLAYING) {
			app_transport_state_set(&app, TRANSPORT_STATE_STOPPED);
			indicator_stopped();
		} else {
			assert(app.sequencer.state == TRANSPORT_STATE_STOPPED);
			app_transport_state_set(&app, TRANSPORT_STATE_PLAYING);
			indicator_playing();
		}
	}

	goto top;
}

int
main(void)
{
	const uint16_t timer_period = 872;
	const float frame_rate = 55 * timer_period;
	app_init(&app, frame_rate, 48);
	app_transport_state_set(&app, TRANSPORT_STATE_PLAYING);
	platform_init(timer_period);
	event_loop();
	return 0;
}
