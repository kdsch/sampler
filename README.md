# Sampler

[![builds.sr.ht status](https://builds.sr.ht/~kdsch/sampler.svg)](https://builds.sr.ht/~kdsch/sampler?)
[![gitlab-ci status](https://gitlab.example.com/kdsch/sampler/badges/master/pipeline.svg)](https://gitlab.com/kdsch/sampler/-/pipelines)

Sampler lets you turn a microcontroller into a music sampler.

## Status

Experimental.

## Build dependencies

|dependency       |version|use                                      |
|:----------------|:------|:----------------------------------------|
|arm-none-eabi-gcc|12.2   |build MCU code                           |
|gcc              |12.2   |build tests and tools                    |
|meson            |1.0.1  |automate builds                          |
|git              |2.39.2 |download dependencies                    |
|libsndfile       |1.5    |update embedded audio files              |
|openocd          |0.12.0 |flash and debug the MCU                  |
|STM32F4-DISCOVERY|       |run the MCU application                  |

## Optional dependencies

|dependency       |version|use                                      |
|:----------------|:------|:----------------------------------------|
|gdb-multiarch    |13.1   |debug MCU code                           |
|cbmc             |5.12   |check assertions against a model         |
|cppcheck         |2.11   |find bugs the compiler cannot            |
|complexity       |1.13   |measure the complexity of code           |

## Usage

	. envsetup.sh
	sampler_setup
	sampler_test
	sampler_flash

1. Load commands into your shell environment by running `. envsetup.sh`.
2. Set up build directories by running `sampler_setup`.
3. Test the code by running `sampler_test`.
4. Flash firmware onto the dev board by running `sampler_flash`.
5. Explore other `sampler_`-prefixed functions by using tab completion.

## Probes

|pin |signal                                              |
|:---|:---------------------------------------------------|
|PA4 |Audio output from DAC peripheral. Beware: DC-coupled|
|PC1 |Buffer processing window                            |

## External documentation

Only the obscure stuff.

- [CBMC/CPROVER manual](https://diffblue.github.io/cbmc/index.html)
- [libopencm3 STM32F4 API documentation](http://libopencm3.org/docs/latest/stm32f4/html/modules.html)
- [Meson build system reference manual](https://mesonbuild.com/Reference-manual.html)
- [N1570 - C11 specification, final draft](https://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf)
- [RM0090 - STM32F4 Reference Manual](https://www.st.com/resource/en/reference_manual/rm0090-stm32f405415-stm32f407417-stm32f427437-and-stm32f429439-advanced-armbased-32bit-mcus-stmicroelectronics.pdf)

## Philosophy

Write testable, portable, simple, readable, correct, efficient code.
